import React, { Fragment } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import Counter from './components/Counter/Counter'
import Profile from './components/Profile/Profile'

function App() {
  return (
    <Fragment>
      <Counter />
      <Profile />
    </Fragment>
  )
}

export default App
