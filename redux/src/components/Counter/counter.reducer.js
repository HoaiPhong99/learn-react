import { createAction, createReducer } from '@reduxjs/toolkit'

export const increase = createAction('counter/increase')
export const decrease = createAction('counter/decrease')

const initState = {
  count: 0
}

// way 1
// const counterReducer = createReducer(initState, {
//   [increase]: (state, action) => {
//     state.count = state.count + 1
//   },
//   [decrease]: (state, action) => {
//     state.count = state.count - 1
//   }
// })

// way 2
const counterReducer = createReducer(initState, builder => {
  builder
    .addCase(increase, (state, action) => {
      state.count = state.count + 1
    })
    .addCase(decrease, (state, action) => {
      state.count = state.count - 1
    })
    .addMatcher(
      action => {
        return action.type.endsWith('increase')
      },
      (state, action) => {
        console.log('handle here')
      }
    )
})
export default counterReducer
