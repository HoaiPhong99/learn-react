import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrease, increase } from './counter.slice'

export default function Counter() {
  const count = useSelector(state => state.counter.count)
  const dispatch = useDispatch()
  return (
    <div>
      <button onClick={() => dispatch(increase())}>increase</button>
      <h1>{count}</h1>
      <button onClick={() => dispatch(decrease())}>decrease</button>
    </div>
  )
}
