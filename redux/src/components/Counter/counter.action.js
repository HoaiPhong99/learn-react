import * as types from './couter.constants'

export const increase = () => ({
  type: types.INCREASE
})

export const decrease = () => ({
  type: types.DECREASE
})
