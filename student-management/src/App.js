import React, { Fragment } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import StudentManagement from './pages/StudentManagement'
import Register from './pages/Register'
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import Home from './pages/Home'
import Profile from './pages/Profile/Profile'
import NotFound from './pages/NotFound'

export const path = {
  home: '/',
  register: '/register',
  students: '/students',
  profile: '/profile',
  profileInfo: '/profile/info/:profileId',
  profilePurchase: '/profile/purchase'
}

function App() {
  return (
    <Fragment>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path={path.home} component={Home} />
          <Route path={path.register} component={Register} />
          <Route path={path.students} component={StudentManagement} />
          <Route path={path.profile} component={Profile} />
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </BrowserRouter>
    </Fragment>
  )
}

export default App
