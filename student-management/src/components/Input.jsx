import React from 'react'
import propTypes from 'prop-types'

export default function Input({ type = 'text', onChange, value, ...props }) {
  const handleChnage = event => {
    const val = event.target.value

    if (type === 'number' && (/^\d+$/.test(val) || val === '')) {
      onChange(val)
    } else {
      onChange(val)
    }
  }

  return <input type={type === 'numer' ? 'text' : type} value={value} onChange={handleChnage} {...props} />
}

Input.propTypes = {
  value: propTypes.oneOfType([propTypes.string, propTypes.number]),
  onChange: propTypes.func.isRequired,
  type: propTypes.string
}
