import React from 'react'
import propTypes from 'prop-types'

export default function Student({ student, handleDelete, handleEdit }) {
  return (
    <li key={student.id} className="list-group-item">
      <span className="me-3">
        {student.name}: {student.age} years
      </span>
      <div className="btn-group">
        <button type="button" className="btn btn-info" onClick={() => handleEdit(student)}>
          Edit
        </button>
        <button type="button" className="btn btn-danger" onClick={() => handleDelete(student.id)}>
          Delete
        </button>
      </div>
    </li>
  )
}

Student.propTypes = {
  student: propTypes.object.isRequired,
  handleDelete: propTypes.func.isRequired,
  handleEdit: propTypes.func.isRequired
}
