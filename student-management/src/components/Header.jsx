import React from 'react'
import { NavLink } from 'react-router-dom'
import { path } from '../App'

export default function Header() {
  return (
    <ul>
      <li>
        <NavLink to={path.home}>Home</NavLink>
      </li>
      <li>
        <NavLink to={path.register}>register</NavLink>
      </li>
      <li>
        <NavLink to={path.students}>students</NavLink>
      </li>
      <li>
        <NavLink to={path.profile}>Profile</NavLink>
      </li>
    </ul>
  )
}
