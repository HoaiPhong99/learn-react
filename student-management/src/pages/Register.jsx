import React from 'react'
import { useForm } from 'react-hook-form'

export default function Register() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues
  } = useForm({
    defaultValues: {
      email: '',
      name: 'Phong',
      nation: 'vietnam',
      hobby: null,
      password: '',
      confirmPassword: ''
    }
  })

  const onSubmit = data => console.log(data)

  const handleClass = (name, baseClass = 'form-control') => `${baseClass} ${errors[name] ? 'is-invalid' : ''}`

  const ErrorMessage = ({ name }) => {
    if (errors[name]) {
      return <div className="invalid-feedback">{errors[name].message}</div>
    }

    return null
  }
  return (
    <div className="container">
      <form noValidate onSubmit={handleSubmit(onSubmit)}>
        <div className="form-floating mb-3">
          <input
            type="email"
            name="email"
            id="email"
            className={handleClass('email')}
            placeholder="name@example.com"
            {...register('email', {
              required: {
                value: true,
                message: 'Email is required'
              },
              validate: {
                email: value =>
                  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) || 'Email invalid'
              }
            })}
          />
          <label htmlFor="email">Email</label>
          <ErrorMessage name="email" />
        </div>
        <div className="form-floating mb-3">
          <input
            type="text"
            name="name"
            id="name"
            className={handleClass('email')}
            placeholder="Phong dep try"
            {...register('name', {
              required: {
                value: true,
                message: 'Name is required'
              },
              minLength: {
                value: 2,
                message: 'Name is 2-150 characters'
              },
              maxLength: {
                value: 150,
                message: 'Name is 2-150 characters'
              }
            })}
          />
          <label htmlFor="name">Name</label>
          <ErrorMessage name="name" />
        </div>
        <div className="mb-3">
          <div className="form-check">
            <input
              type="radio"
              name="nation"
              id="vietnam"
              value="vietnam"
              className={handleClass('nation', 'form-check-input')}
              {...register('nation', {
                required: {
                  value: true,
                  message: 'Nation is required'
                }
              })}
            />
            <label htmlFor="vietnam" className="form-check-label">
              Việt Name
            </label>
          </div>
          <div className="form-check">
            <input
              type="radio"
              name="nation"
              id="aboard"
              value="aboard"
              className={handleClass('nation', 'form-check-input')}
              {...register('nation', {
                required: {
                  value: true,
                  message: 'Nation is required'
                }
              })}
            />
            <label htmlFor="aboard" className="form-check-label">
              Aboard
            </label>
            <ErrorMessage name="nation" />
          </div>
        </div>
        <div className="mb-3">
          <div className="form-check">
            <input
              type="checkbox"
              className={handleClass('hobby', 'form-check-input')}
              name="hobby"
              id="gym"
              value="gym"
              {...register('hobby', {
                required: {
                  value: true,
                  message: 'Hobby is required'
                }
              })}
            />
            <label htmlFor="gym" className="form-check-label">
              Gym
            </label>
          </div>
          <div className="form-check">
            <input
              type="checkbox"
              className={handleClass('hobby', 'form-check-input')}
              name="hobby"
              id="other"
              value="other"
              {...register('hobby', {
                required: {
                  value: true,
                  message: 'Hobby is required'
                }
              })}
            />
            <label htmlFor="other" className="form-check-label">
              Other
            </label>
            <ErrorMessage name="hobby" />
          </div>
        </div>
        <div className="mb-3">
          <select
            name="sex"
            className={handleClass('sex', 'form-select')}
            {...register('sex', {
              required: {
                value: true,
                message: 'Sex is required'
              }
            })}
          >
            <option value="">Sex</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
            <option value="3">Other</option>
          </select>
          <ErrorMessage name="sex" />
        </div>
        <div className="form-floating mb-3">
          <input
            type="password"
            name="password"
            id="password"
            className="form-control"
            className={handleClass('password')}
            placeholder="***"
            {...register('password', {
              required: {
                value: true,
                message: 'Password is required'
              },
              minLength: {
                value: 6,
                message: 'Password is 6-150 characters'
              },
              maxLength: {
                value: 150,
                message: 'Password is 6-150 characters'
              }
            })}
          />
          <label htmlFor="password">Password</label>
          <ErrorMessage name="password" />
        </div>
        <div className="form-floating mb-3">
          <input
            type="password"
            name="confirmPassword"
            id="confirmPassword"
            className={handleClass('confirmPassword')}
            placeholder="***"
            {...register('confirmPassword', {
              required: {
                value: true,
                message: 'Confirm Password is required'
              },
              minLength: {
                value: 6,
                message: 'Confirm Password is 6-150 characters'
              },
              maxLength: {
                value: 150,
                message: 'Confirm Password is 6-150 characters'
              },
              validate: {
                samePassword: value => value === getValues('password') || 'Password not match'
              }
            })}
          />
          <label htmlFor="confirmPassword">Confirm Password</label>
          <ErrorMessage name="confirmPassword" />
        </div>
        <button type="submit" className="btn btn-primary">
          Register
        </button>
      </form>
    </div>
  )
}
