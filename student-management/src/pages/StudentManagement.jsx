import React, { useState } from 'react'
import Input from '../components/Input'
import Student from '../components/Student'
import useQueryString from '../hook/useQueryString'

export default function StudentManagement() {
  const [name, setName] = useState('')
  const [age, setAge] = useState('')
  const [students, setStudents] = useState([])
  const [currentStudent, setCurrentStudent] = useState(null)
  const queryString = useQueryString()

  console.log(queryString)
  const handleChange = setFunc => value => setFunc(value)

  const handleInsertOrUpdate = e => {
    e.preventDefault()
    if (name && age) {
      if (currentStudent) {
        const _students = students.map(student => {
          if (student.id === currentStudent.id) {
            return {
              name,
              age,
              id: student.id
            }
          }
          return student
        })
        setStudents(_students)
      } else {
        setStudents(prevStudents => [
          ...prevStudents,
          {
            id: new Date().toISOString(),
            name: name,
            age: age
          }
        ])
      }
      handleCancel()
    }
  }

  const handleDelete = id => {
    const index = students.findIndex(student => student.id === id)
    const _students = [...students]
    _students.splice(index, 1)
    setStudents(_students)
  }

  const handleEdit = student => {
    setCurrentStudent(student)
    setName(student.name)
    setAge(student.age)
  }

  const handleCancel = () => {
    setName('')
    setAge('')
    setCurrentStudent(null)
  }
  return (
    <div>
      <h1>Student Managent</h1>
      <form className="mb-3" onSubmit={handleInsertOrUpdate}>
        <div className="mb-3">
          <label htmlFor="name" className="form-label">
            Name:
          </label>
          <Input
            type="text"
            className="form-control"
            id="name"
            placeholder="Input Name"
            value={name}
            onChange={handleChange(setName)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="age" className="form-label">
            Age:
          </label>
          <Input
            type="number"
            className="form-control"
            id="age"
            placeholder="Input Age"
            value={age}
            onChange={handleChange(setAge)}
          />
        </div>
        <div className="">
          <button className="btn btn-primary" type="submit">
            {currentStudent ? 'Update' : 'Add'}
          </button>
          {currentStudent && (
            <button className="btn btn-danger" type="button" onClick={handleCancel}>
              cancel
            </button>
          )}
        </div>
      </form>

      <ul className="list-group">
        {students.map(student => (
          <Student student={student} key={student.id} handleDelete={handleDelete} handleEdit={handleEdit} />
        ))}
      </ul>
    </div>
  )
}
