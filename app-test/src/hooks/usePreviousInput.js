import { useEffect, useRef } from 'react'

const usePreviousInput = value => {
  const ref = useRef()

  useEffect(() => {
    ref.current = value
  }, [value])

  return ref.current
}

export default usePreviousInput
