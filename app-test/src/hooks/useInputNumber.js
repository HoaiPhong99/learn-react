import { useState } from 'react'

const useInputNumber = initialState => {
  const [value, setValue] = useState(initialState)

  const handleChange = event => {
    const value = event.target.value

    if (/\d+$/.test(value) || value === '') {
      setValue(value)
    }
  }

  return [value, handleChange]
}

export default useInputNumber
