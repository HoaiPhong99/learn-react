import React, { useCallback, useMemo, useState } from 'react'
import './App.css'
import TestFunctionComponent from './TestFunctionComponent'

function App1() {
  const [count, setCount] = useState(0)
  const profile = useMemo(
    () => ({
      name: 'Phong',
      age: 555
    }),
    []
  )

  const handleClick = useCallback(value => console.log(`value`, value), [])

  console.log('App')

  return (
    <div className="App">
      <button onClick={() => setCount(count => count + 1)}>Click</button>
      <TestFunctionComponent data={profile} handleClick={handleClick}></TestFunctionComponent>
    </div>
  )
}

export default App1
