import React, { createContext, useContext } from 'react'

const UserContext = createContext()

export const useUserContext = () => useContext(UserContext)

const UserContextProvider = ({ children }) => {
  const user = {
    name: 'Phuong',
    age: 24
  }

  return <UserContext.Provider value={user}>{children}</UserContext.Provider>
}

export default UserContextProvider
