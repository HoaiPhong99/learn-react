import React, { forwardRef, memo, useRef, useState } from 'react'

const initStateProfile = {
  name: 'Phong',
  age: 23
}

const Input = forwardRef((props, ref) => <input {...props} ref={ref} />)

const TestFunctionComponent = memo(props => {
  const { handleClick } = props
  const [profile, setProfile] = useState(() => initStateProfile)
  const [inputs, setInputs] = useState(initStateProfile)
  const bRef = useRef(null)
  const inputRef = useRef()
  console.log('TestFunctionComponent', props)

  const onChangeInput = e => setInputs(prevState => ({ ...prevState, [e.target.name]: e.target.value }))

  const onUpdate = () => {
    setProfile(prevState => ({ ...prevState, ...inputs }))
  }

  const changeColor = () => {
    bRef.current.style.color = 'red'
  }

  const changeBorder = () => {
    inputRef.current.style.borderColor = 'red'
  }

  return (
    <React.Fragment>
      <div>
        <button onClick={changeColor}>change color</button>
        <b ref={bRef}>Information</b>
        <p>Name: {profile.name}</p>
        <p>Age: {profile.age}</p>
      </div>
      <table border="1">
        <tbody>
          <tr>
            <td>name</td>
            <td>
              <input type="text" name="name" onChange={onChangeInput} value={inputs.name || ''} />
            </td>
          </tr>
          <tr>
            <td>Age</td>
            <td>
              <input type="text" name="age" onChange={onChangeInput} value={inputs.age || ''} />
            </td>
          </tr>
          <tr>
            <td>
              <button onClick={changeBorder}>change border</button>
            </td>
            <td>
              <Input ref={inputRef} />
            </td>
          </tr>
          <tr>
            <td colSpan="2">
              <button type="button" onClick={onUpdate}>
                update
              </button>
              <button type="button" onClick={() => handleClick(123)}>
                click
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </React.Fragment>
  )
})

export default TestFunctionComponent
