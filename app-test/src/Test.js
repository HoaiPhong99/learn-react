import React, { Component } from 'react'

export default class Test extends Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0
    }
  }

  onClick = () => {
    this.setState(prevstate => ({ count: prevstate.count + 4 }))
  }

  render() {
    return (
      <div>
        <div>count: {this.state.count}</div>
        <button onClick={this.onClick}>click</button>
      </div>
    )
  }
}
