import React from 'react'
import useInputNumber from './hooks/useInputNumber'
import usePreviousInput from './hooks/usePreviousInput'

function CustomHooks() {
  const [valueNumber, handleChange] = useInputNumber('')
  const prevInputNumber = usePreviousInput(valueNumber)

  console.log(prevInputNumber)

  return (
    <div>
      <input type="text" value={valueNumber} onChange={handleChange} />
    </div>
  )
}

export default CustomHooks
