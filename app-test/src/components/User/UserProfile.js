import { useUserContext } from '../../context/UserContext'

function UserProfile() {
  const value = useUserContext()
  return (
    <div>
      <p>Name: {value.name}</p>
      <p>Age: {value.age}</p>
    </div>
  )
}

export default UserProfile
