import React from 'react'
import UserProfile from './UserProfile'

function User() {
  return (
    <div>
      <UserProfile />
    </div>
  )
}

export default User
