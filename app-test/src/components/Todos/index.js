import React, { useReducer, useState } from 'react'

const reducer = (state, action) => {
  switch (action.type) {
    case 'submit':
      return [
        ...state,
        {
          value: action.payload,
          id: new Date().toISOString()
        }
      ]
    case 'delete':
      return state.filter(todo => todo.id !== action.payload)

    default:
      break
  }
}
function Todos() {
  // const [todos, setTodos] = useState([])
  const [value, setValue] = useState('')
  const [todos, dispatch] = useReducer(reducer, [])
  const handSubmit = e => {
    e.preventDefault()
    dispatch({ type: 'submit', payload: value })
    setValue('')
  }
  const handleDelete = id => {
    dispatch({ type: 'delete', payload: id })
  }
  return (
    <div>
      <form onSubmit={handSubmit}>
        <input type="text" onChange={e => setValue(e.target.value)} value={value} />
      </form>
      <ul>
        {todos &&
          todos.map(todo => (
            <li key={todo.id}>
              {todo.value} <button onClick={() => handleDelete(todo.id)}>delete</button>
            </li>
          ))}
      </ul>
    </div>
  )
}

export default Todos
